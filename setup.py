from setuptools import setup, find_packages

setup(name="ob_pdf_client",
      version="2",
      url="https://gitlab.com/obuilds/labs/ob_pdf_client_python",
      author="oBuilds, LLC",
      author_email="k@obuilds.com",
      packages=find_packages(),
      include_package_data=True,
      )
