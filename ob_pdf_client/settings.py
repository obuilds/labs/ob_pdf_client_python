import os


API_HOST = os.getenv("PDF_API_HOST", "https://pdf.obuilds.com")

API_KEY = os.getenv("PDF_API_KEY")
