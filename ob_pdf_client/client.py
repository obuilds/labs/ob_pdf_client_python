from kbde.api_client import client as api_client

from . import settings

import time, datetime


class ClientBase(api_client.Client):
    headers = {
        "Authorization": "Token {api_key}",
    }
    host = settings.API_HOST

    def __init__(self, *args, api_key=None, **kwargs):
        api_key = api_key or settings.API_KEY

        assert api_key, ("Must pass the `api_key` parameter when instantiating, or set the "
                         "`PDF_API_KEY` environment variable")

        super().__init__(*args, api_key=api_key, **kwargs)


class PublicPdfTemplateList(ClientBase):
    path = "/api/v1/document/pdf/template/public"


class PrivatePdfTemplateList(ClientBase):
    path = "/api/v1/document/pdf/template/private"
    

class PdfTemplateDetail(ClientBase):
    path = "/api/v1/document/pdf/template/{id}"


class Create(ClientBase):
    path = "/api/v1/document/create"


class PacketCreate(ClientBase):
    path = "/api/v1/document/packet/create"


class Detail(ClientBase):
    path = "/api/v1/document/{id}"


class Client:
    endpoint_classes = [
        PublicPdfTemplateList,
        PrivatePdfTemplateList,
        PdfTemplateDetail,
        Create,
        PacketCreate,
        Detail,
    ]

    def __init__(self, api_key=None):
        self.instantiate_clients(api_key)
        
    def instantiate_clients(self, api_key):
        for endpoint_class in self.endpoint_classes:
            client = endpoint_class(api_key=api_key)
            setattr(self, client.__class__.__name__, client)

    def render_pdf_document(self, pdf_template_name, data, name=None, interval=1):
        return self.render_document(interval,
                                    name=name,
                                    document_type="pdf",
                                    pdf_template_name=pdf_template_name,
                                    data=data)

    def render_html_document(self, html, name=None, interval=1):
        return self.render_document(interval,
                                    document_type="html",
                                    name=name,
                                    html=html)

    def render_document(self, interval, **kwargs):
        """
        Render a document
        Blocks until the result is finished rendering
        Checks for the result at `interval` seconds
        """

        # Create the rendered document
        rendered_document = self.Create.post(**kwargs)

        return self.poll_for_result(rendered_document, interval)

    def render_packet(self, document_list, name=None, interval=1):
        # Create rendered packet
        rendered_packet = self.PacketCreate.post(document_list=document_list,
                                                 name=name)

        return self.poll_for_result(rendered_packet, interval)

    def poll_for_result(self, rendered_document, interval=1, timeout=15):
        start_time = datetime.datetime.now()

        while rendered_document["status"] != "complete":
            time.sleep(interval)
            rendered_document = self.Detail.get(id=rendered_document["id"])

            if (datetime.datetime.now() - start_time).seconds > timeout:
                raise self.RenderTimeout()

        return rendered_document

    class RenderTimeout(Exception):
        pass
